/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple.app;

import cviko03.*;
import java.util.*;

/**
 *
 * @author Honza
 */
public class SimpleApp {

    /**
     * @param args the command line arguments
     */
    public static Map<String, Object> objMap = new Hashtable<>();
    public static void main(String[] args) {

        boolean cont = true;
        Scanner sc = new Scanner(System.in);
        while (cont) {            
            
            System.out.println("1 - vlozit novy objekt\n2 - vyhledat nejblizsi objekt\n3 - ukoncit program");
            switch (sc.nextInt()){
                case 1: System.out.println("case 1");
                        chooseObj(sc);
                        break;
                case 2: System.out.println("case 2");
                        searchClosestObj(sc);
                        break;
                case 3: System.out.println("konec");
                        cont = false;
                        break;
                default:System.out.println("Chyba, opakujte akci");   
                        break;
            }
        }
        // TODO code application logic here
    }
    
    public static void chooseObj(Scanner sc){
        String name = null;
        double aX,aY,bX,bY, d;
        System.out.print("Zadejte typ objektu: \n\t1 - Point\n\t2 - Line\n\t3 - Rectangle\n\t4 - Square\n\t5 - Circle\n");
        int type = sc.nextInt();
        while(!(type == 1 || type == 2 || type == 3 || type == 4 || type == 5)){
            System.out.print("Chybne zadani, opakujte akci\nZadejte typ objektu: \n\t1 - Point\n\t2 - Line\n\t3 - Rectangle\n\t4 - Square\n\t5 - Circle\n");
            type = sc.nextInt();
        }
        System.out.print("zadejte jmeno objektu: ");
        name = sc.next();
        while(objMap.containsKey(name)){
            System.out.println("\"" + name + "\"" + " uz existuje, zadejte jiny nazev: ");
            name = sc.next();
        }
        System.out.println("zadali jste jmeno: " + name);
        switch (type){
            case 1: System.out.print("zadejte souradnici X(ve formatu napr. 1,0): ");
            double x = sc.nextDouble();
            System.out.print("zadejte souradnice Y(ve formatu napr. 2,0): ");
            double y = sc.nextDouble();
                    objMap.put(name, new Point(x, y));
                    break;
            case 2: System.out.print("zadejte souradnice(ve formatu bodA bodB)\nzadejte X souradnici bodu A: ");
                    aX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu A: ");
                    aY = sc.nextDouble();
                    System.out.print("zadejte X souradnici bodu B: ");
                    bX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu B: ");
                    bY = sc.nextDouble();
                    objMap.put(name, new Line(new Point(aX, aY), new Point(bX, bY)));
                    break;
            case 3: System.out.print("zadejte souradnice(ve formatu bodA bodB\nzadejte X souradnici bodu A: ");
                    aX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu A: ");
                    aY = sc.nextDouble();
                    System.out.print("zadejte X souradnici bodu B: ");
                    bX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu B: ");
                    bY = sc.nextDouble();
                    objMap.put(name, new Rectangle(new Point(aX, aY), new Point(bX, bY)));
                    break;
            case 4: System.out.print("zadejte souradnice(ve formatu bodA delkasttrany\nzadejte X souradnici bodu A: ");
                    aX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu A: ");
                    aY = sc.nextDouble();
                    System.out.print("zadejte delku strany: ");
                    d = sc.nextDouble();
                    objMap.put(name, new Square(new Point(aX,aY),d));
                    break;
            case 5: System.out.print("zadejte souradnice(ve formatu bodA polomer)\nzadejte X souradnici bodu A: ");
                    aX = sc.nextDouble();
                    System.out.print("zadejte Y souradnici bodu A: ");
                    aY = sc.nextDouble();
                    System.out.print("zadejte delku polomer: ");
                    d = sc.nextDouble();
                    objMap.put(name, new Circle(new Point(aX,aY),d));
                    break;
            default: System.out.println("chyba in chooseObj");
        }
        
    }
    
    public static void searchClosestObj(Scanner sc){
        double x,y;
        double tmpDistance;
        System.out.print("zadejte souradnice\nX: ");
        x = sc.nextDouble();
        System.out.print("Y:");
        y = sc.nextDouble();
        Point p = new Point(x, y);
        
        for (String val1 : objMap.keySet()) {
            if(objMap.get(val1).getClass() ==  Point.class){
                objMap.get(val1);
            }
        }
    }

}
